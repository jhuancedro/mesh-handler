cl__1 = 1;
Point(1) = {0, 0, 0, 0};
Point(2) = {0, 1, 0, 0};
Point(3) = {1, 1, 0, 0};
Point(4) = {1, 0, 0, 0};
Line(1) = {4, 3};
Line(2) = {3, 2};
Line(3) = {2, 1};
Line(4) = {1, 4};
Line Loop(6) = {4, 1, 2, 3};
Plane Surface(6) = {6};

Transfinite Line {4, 1, 2, 3} = 3;
Transfinite Surface{6};
Recombine Surface{6};
