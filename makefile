SRC 		= src
BUILD 		= build
FORTRAN_EXS = fortran90_examples

include ${PETSC_DIR}/lib/petsc/conf/variables
include ${PETSC_DIR}/lib/petsc/conf/rules

ex1: ${SRC}/ex1.o   chkopts
	-${CLINKER} -o ${BUILD}/ex1 ex1.o  ${PETSC_DM_LIB}
	${RM} -f ${SRC}/ex1.o

main: ${SRC}/main.o ${SRC}/mesh.o chkopts 
	-${CLINKER} -o ${BUILD}/main ${SRC}/main.o ${SRC}/mesh.o ${PETSC_DM_LIB}
	${RM} -f ${SRC}/*.o
	
dmplex01: ${SRC}/dmplex01.o   chkopts
	-${CLINKER} -o ${BUILD}/dmplex01 ${SRC}/dmplex01.o  ${PETSC_DM_LIB}
	${RM} -f ${SRC}/dmplex01.o
	
dmplex02: ${SRC}/dmplex02.o   chkopts
	-${CLINKER} -o ${BUILD}/dmplex02 ${SRC}/dmplex02.o  ${PETSC_DM_LIB}
	${RM} -f ${SRC}/dmplex02.o

dmplex03: ${SRC}/dmplex03.o   chkopts
	-${CLINKER} -o ${BUILD}/dmplex03 ${SRC}/dmplex03.o  ${PETSC_DM_LIB}
	${RM} -f ${SRC}/dmplex03.o

teste_dmplex_box: ${FORTRAN_EXS}/teste_dmplex_box.o  chkopts
	-${FLINKER} -o ${BUILD}/teste_dmplex_box teste_dmplex_box.o  ${PETSC_DM_LIB}
	${RM} -f ${SRC}/teste_dmplex_box.o

teste_dmplex_cell_list: ${FORTRAN_EXS}/teste_dmplex_cell_list.o  chkopts
	-${FLINKER} -o ${BUILD}/teste_dmplex_cell_list teste_dmplex_cell_list.o  ${PETSC_DM_LIB}
	${RM} -f ${FORTRAN_EXS}/teste_dmplex_cell_list.o

#include ${PETSC_DIR}/lib/petsc/conf/test
