Projeto de manipulador de malha desenvolvido para a disciplina de Engenharia de Software do PPGMC da UFJF.

Try "$ make main && build/main"

Mesh example file are folded in "mesh_examples/".

In "src/", dmplex01, dmplex02, dmplex03 are code sketches.

Mesh class support some methods with STL vector.
