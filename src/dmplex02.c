static char help[] = "Teste 01\n\n";

typedef enum {false, true} bool;

#include <stdio.h>
#include <string.h>
#include <petscdmplex.h>

void listFunction1(
  PetscErrorCode (*DMPlexGetFunction    )(DM, PetscInt, const PetscInt**),
  PetscErrorCode (*DMPlexGetFunctionSize)(DM, PetscInt, PetscInt*),
  const char functionName[30],
  DM dm, PetscInt p, bool out){
	const PetscInt *set;
	PetscInt size, i;
	
	DMPlexGetFunctionSize(dm, p, &size);
	DMPlexGetFunction(dm, p, &set);
	
	if(out)	printf("%s of %d: ", functionName, p);
		
	printf("{");
	for(i = 0; i < size; i++)
		printf("%d ", set[i]);	printf("} ");
	if(out) printf("\n");	
}

void displayArray(const PetscInt* array, int n){
	int i;
	for(i=0; i<n; i++)
		printf("%d, ", array[i]);
}

void listFunction2(
  PetscErrorCode (*DMPlexGetFunction)(DM, PetscInt, const PetscInt*, PetscInt*, const PetscInt**),
  const char functionName[30],
  DM dm, PetscInt numPoints, const PetscInt* points){
	const PetscInt *set;
	PetscInt size, i;
	
	DMPlexGetFunction(dm, numPoints, points, &size, &set);
	
	printf("%s of ", functionName);
	displayArray(points, numPoints);
	printf("{");
	for(i = 0; i < size*2; i++)
		printf("%d ", set[i]);	
	printf("} ");
}

/* list cone of point p */
void listCone(DM dm, PetscInt p, bool out){
	listFunction1(DMPlexGetCone, DMPlexGetConeSize, "Cone", dm, p, out);
}

/* list support of point p */
void listSupport(DM dm, PetscInt p, bool out){
	listFunction1(DMPlexGetSupport, DMPlexGetSupportSize, "Support", dm, p, out);
}

/* list meet of point array 'points' */
void listMeet(DM dm, PetscInt numPoints, const PetscInt* points){
	listFunction2(DMPlexGetMeet, "Meet", dm, numPoints, points);
}

/* list transitive closure of a point t*/
void listClosure(DM dm, PetscInt p){
	PetscInt *set;
	PetscInt size, i;
	
	DMPlexGetTransitiveClosure(dm, p, PETSC_TRUE, &size, &set);
	printf("Transitive Closure of %d: ", p);
		
	printf("{");
	for(i = 0; i < size*2; i+=2)	/* ignore point orientations */
		printf("%d ", set[i]);	printf("} ");
	printf("\n");	
}

/* Also set degrees of freedom */
void listHeights(DM dm, PetscSection section){	
	/* we can lay out data for a continuous Galerkin P_3 inite element method */
	PetscInt pStart, pEnd, cStart, cEnd, c, vStart, vEnd, v, eStart, eEnd, e;

	DMPlexGetChart(dm, &pStart, &pEnd);
	DMPlexGetHeightStratum(dm, 0, &cStart, &cEnd);
	DMPlexGetHeightStratum(dm, 1, &eStart, &eEnd);
	DMPlexGetDepthStratum (dm, 0, &vStart, &vEnd);
	PetscSectionSetChart(section, pStart, pEnd);
	
	printf("\nHeight 0: ");
	for(c = cStart; c < cEnd; ++c){
		PetscSectionSetDof(section, c, 1);	// Sets the dof (1) associated with a given point (c). 
		printf("%d   ", c);
	}
	
	printf("\nHeight 1: ");
	for(v = vStart; v < vEnd; ++v){
		PetscSectionSetDof(section, v, 1);	// Sets the dof (1) associated with a given point (v). 
		printf("%d   ", v);
	}
	
	printf("\nDepth  0: ");
	for(e = eStart; e < eEnd; ++e){
		PetscSectionSetDof(section, e, 2);	// Sets the dof (2) associated with a given point (e). 
		printf("%d   ", e);
	}printf("\n");
}

/* Also set degrees of freedom */
void listDepths(DM dm, PetscSection section){	
	PetscInt pStart, pEnd, p, i;
	
	for(i=0; i<3; i++){
		DMPlexGetDepthStratum(dm, i, &pStart, &pEnd);
		
		printf("\nDepth  %d: ", i);
		for(p = pStart; p < pEnd; ++p){
			printf("%d ", p);
			if(i > 0){
				listCone(dm, p, false);
			}
		}
		printf("\n");
	}	
}

int main(int argc,char **argv){
	DM             dm;
	PetscSection   section;
	PetscInt       dim = 1;
	PetscBool      interpolate = PETSC_TRUE;
	PetscErrorCode ierr;

	ierr = PetscInitialize(&argc, &argv, NULL,help);if (ierr) return ierr;
	ierr = PetscOptionsGetInt(NULL,NULL, "-dim", &dim, NULL);CHKERRQ(ierr);
	
	/* Create a mesh: actually import from gmsh file */
	DMPlexCreateFromFile(PETSC_COMM_WORLD, "malha01.msh", interpolate, &dm);
	
	PetscInt pStart, pEnd;
	DMPlexGetChart(dm, &pStart, &pEnd);
	printf("malha importada com points de %d a %d \n", pStart, pEnd);
	
	/* 10.2.1 - DATA LAYOUT */	
	DMGetDefaultGlobalSection(dm, &section);
	/* list heights and set degrees of freedom */
	listDepths(dm, section);
	
	PetscSectionSetUp(section);
	//DMGetLocalVector(dm, &localVec);
	//DMGetGlobalVector(dm, &globalVec);
	/* Now a PETSc local vector can be created manually using this layout */
	PetscInt n;
	PetscSectionGetStorageSize(section, &n);	// Return the size of an array or local Vec capable of holding all the degrees of freedom. 
	
	Vec localVec, globalVec;
	VecCreate(PETSC_COMM_WORLD, &localVec );		// Creates an empty vector object.
	VecCreate(PETSC_COMM_WORLD, &globalVec);		// Creates an empty vector object.
	VecSetSizes(localVec, n, PETSC_DETERMINE);	// Sets the local and global sizes, and checks to determine compatibility
	VecSetFromOptions(localVec);				//Configures the vector from the options database.
	/// If you never call VecSetType() or VecSetFromOptions() it will generate an error when you try to use the vector
	
	/* though it is usually easier to use the DM directly, which also provides global vectors */
	DMSetDefaultSection(dm, section);
	DMGetLocalVector(dm, &localVec);
	DMGetGlobalVector(dm, &globalVec);
	
	listCone(dm, 9, true);
	listCone(dm, 1, true);
	listSupport(dm, 9, true);
	listSupport(dm, 10, true);
	//listClosure(dm, 17);
	/*
	PetscInt *set;
	PetscInt size, i, p=17;
	
	DMPlexGetTransitiveClosure(dm, p, PETSC_TRUE, &size, &set);
	printf("Transitive Closure of %d: ", p);
		
	printf("{");
	for(i = 0; i < size*2; i+=2)	/* ignore point orientations *
		printf("%d ", set[i]);	printf("} ");
	printf("\n");	*/
	if(false){
		const PetscInt v[2] = {18, 19};
		listMeet(dm, 2, v);
	}
	
	//ierr = DMDestroy(&dm);CHKERRQ(ierr); // Segmentation fault (core dumped)
	ierr = PetscFinalize();
	return ierr;
}
