/* DMPlexSetCone NAO ESTA FUNCIONANDO
 * novo array cone sobrescrevw o cone do no anterior */

static char help[] = "Teste 01\n\n";

#include <petscdmplex.h>

void listCone(DM dm, PetscInt c){
	const PetscInt* cone;
	DMPlexGetCone(dm, c, &cone);
	printf("%d %d %d %d %d \n", cone[0], cone[1], cone[2], cone[3], cone[4]);
}

int main(int argc,char **argv){
	DM             dm;
	PetscSection   section;
	PetscInt       dim = 1;
	PetscBool      interpolate = PETSC_TRUE;
	PetscErrorCode ierr;

	ierr = PetscInitialize(&argc, &argv, NULL,help);if (ierr) return ierr;
	ierr = PetscOptionsGetInt(NULL,NULL, "-dim", &dim, NULL);CHKERRQ(ierr);
	/* Create a mesh */
	ierr = DMPlexCreateBoxMesh(PETSC_COMM_WORLD, dim, PETSC_TRUE, NULL, NULL, NULL, NULL, interpolate, &dm);CHKERRQ(ierr);
	
	/* Start of Chapter 10 example */
	/* First, we declare the set of points present in a mesh */
	ierr = DMPlexSetChart(dm, 0, 11);CHKERRQ(ierr);	// [0, 11)
	
	/* Setting sizes of the  covering relations for preallocating */
	DMPlexSetConeSize(dm,  0, 3);
	DMPlexSetConeSize(dm,  1, 3);
	DMPlexSetConeSize(dm,  6, 2);
	DMPlexSetConeSize(dm,  7, 2);
	DMPlexSetConeSize(dm,  8, 2);
	DMPlexSetConeSize(dm,  9, 2);
	DMPlexSetConeSize(dm, 10, 2);
	DMSetUp(dm);
	
	/* Actually setting the covering relations */
	const PetscInt cone00[] = {6, 7, 8},
				   cone01[] = {7, 9, 8},
				   cone06[] = {2, 3},
				   cone07[] = {3, 4},
				   cone08[] = {4, 2},
				   cone09[] = {4, 5},
				   cone10[] = {5, 3};
	
	DMPlexSetCone(dm,  0, cone00); //DMPlexSetCone(dm,  0, [6, 7,  8]);
	DMPlexSetCone(dm,  1, cone01); //DMPlexSetCone(dm,  1, [7, 9, 10]);
	//DMPlexSetCone(dm,  6, cone06); //DMPlexSetCone(dm,  6, [2, 3]);
	//DMPlexSetCone(dm,  7, cone07); //DMPlexSetCone(dm,  7, [3, 4]);
	//DMPlexSetCone(dm,  8, cone08); //DMPlexSetCone(dm,  8, [4, 2]);
	//DMPlexSetCone(dm,  9, cone09); //DMPlexSetCone(dm,  9, [4, 5]);
	//DMPlexSetCone(dm, 10, cone10); //DMPlexSetCone(dm, 10, [5, 3]);
	
	/*	There is also an API for the dual relation, using DMPlexSetSupportSize() and DMPlexSetSupport(), 
	 * but this can be calculated automatically by calling: */
	//DMPlexSymmetrize(dm); // Error, because: "Supports were already setup in this DMPlex"
	
	/*In order to support efficient queries, we also want to construct fast search structures and indices
	 * for the different types of points, which is done using*/
	DMPlexStratify(dm);

	/* 10.2.1 - DATA LAYOUT */	
	DMGetDefaultGlobalSection(dm, &section);
	/* we can lay out data for a continuous Galerkin P_3 inite element method */
	PetscInt pStart, pEnd, cStart, cEnd, c, vStart, vEnd, v, eStart, eEnd, e;

	DMPlexGetChart(dm, &pStart, &pEnd);
	DMPlexGetHeightStratum(dm, 0, &cStart, &cEnd);
	DMPlexGetHeightStratum(dm, 1, &eStart, &eEnd);
	DMPlexGetDepthStratum(dm, 0, &vStart, &vEnd);
	PetscSectionSetChart(section, pStart, pEnd);
	for(c = cStart; c < cEnd; ++c){
		PetscSectionSetDof(section, c, 1);	// Sets the dof (1) associated with a given point (c). 
		printf("%d   ", c);
	}printf("\n");
	for(v = vStart; v < vEnd; ++v){
		PetscSectionSetDof(section, v, 1);	// Sets the dof (1) associated with a given point (v). 
		printf("%d   ", v);
	}printf("\n");
	for(e = eStart; e < eEnd; ++e){
		PetscSectionSetDof(section, e, 2);	// Sets the dof (2) associated with a given point (e). 
		printf("%d   ", e);
	}printf("\n");
	PetscSectionSetUp(section);
	//DMGetLocalVector(dm, &localVec);
	//DMGetGlobalVector(dm, &globalVec);
	/* Now a PETSc local vector can be created manually using this layout */
	PetscInt n;
	PetscSectionGetStorageSize(section, &n);	// Return the size of an array or local Vec capable of holding all the degrees of freedom. 
	
	Vec localVec, globalVec;
	VecCreate(PETSC_COMM_WORLD, &localVec );		// Creates an empty vector object.
	VecCreate(PETSC_COMM_WORLD, &globalVec);		// Creates an empty vector object.
	VecSetSizes(localVec, n, PETSC_DETERMINE);	// Sets the local and global sizes, and checks to determine compatibility
	VecSetFromOptions(localVec);				//Configures the vector from the options database.
	/// If you never call VecSetType() or VecSetFromOptions() it will generate an error when you try to use the vector
	
	/* though it is usually easier to use the DM directly, which also provides global vectors */
	DMSetDefaultSection(dm, section);
	DMGetLocalVector(dm, &localVec);
	DMGetGlobalVector(dm, &globalVec);
	
	listCone(dm, 0);
	listCone(dm, 1);
	
	
	//ierr = DMDestroy(&dm);CHKERRQ(ierr); // Segmentation fault (core dumped)
	ierr = PetscFinalize();
	return ierr;
}
