/**---------------------------------------------------------------------
 *    Jhuan B. S. Cedro, June, 2018
 *--------------------------------------------------------------------*/
 
 #include "mesh.h"

/* TODO: check which arrays actually must be deleted */

Mesh::Mesh(const char* file){
	ierr = DMPlexCreateFromFile(PETSC_COMM_WORLD, file, PETSC_TRUE, &dm); //CHKERRQ(ierr);
	PetscInt pStart, pEnd;
	DMPlexGetChart(dm, &pStart, &pEnd);
	printf("Imported mesh with points from %d to %d \n", pStart, pEnd);
}

Mesh::~Mesh(){
	//ierr = DMDestroy(&dm); // CHKERRQ(ierr); // Segmentation fault (core dumped)
}

/* get CONE of point p */
PetscErrorCode Mesh::getCone(const PetscInt p, const PetscInt** set, PetscInt *size){
	ierr = DMPlexGetConeSize(dm, p, size); CHKERRQ(ierr);
	ierr = DMPlexGetCone(dm, p, set); CHKERRQ(ierr);
	
	return ierr;
}

/* get SUPPORT of point p */
PetscErrorCode Mesh::getSupport(const PetscInt p, const PetscInt** set, PetscInt *size){
	ierr = DMPlexGetSupportSize(dm, p, size); CHKERRQ(ierr);
	ierr = DMPlexGetSupport(dm, p, set); CHKERRQ(ierr);
	
	return ierr;
}

/* get MEET of point array 'points' */
PetscErrorCode Mesh::getMeet(const PetscInt* points, const PetscInt numPoints, const PetscInt** set, PetscInt *size){
	ierr = DMPlexGetMeet(dm, numPoints, points, size, set); CHKERRQ(ierr);
	for(PetscInt i=1; i < *size; i++)	set[i] = set[2*i];
	return ierr;
}

/* get JOIN of point array 'points' */
PetscErrorCode Mesh::getJoin(const PetscInt* points, const PetscInt numPoints, const PetscInt** set, PetscInt *size){
	ierr = DMPlexGetJoin(dm, numPoints, points, size, set); CHKERRQ(ierr);
	for(PetscInt i=1; i < *size; i++)	set[i] = set[2*i];
	return ierr;
}

/* get TRANSITIVE CLOSURE of a point p*/
PetscErrorCode Mesh::getClosure(const PetscInt p, const PetscBool direct, PetscInt** set, PetscInt *size){
	ierr = DMPlexGetTransitiveClosure(dm, p, direct, size, set); CHKERRQ(ierr);
	for(PetscInt i=1; i < *size; i++)	(*set)[i] = (*set)[2*i];
	//displayArray(*set, *size, 2);
	return ierr;
}

/* get DIRECT TRANSITIVE CLOSURE of a point p */
PetscErrorCode Mesh::getDirectClosure(const PetscInt p, PetscInt** set, PetscInt *size){
	return getClosure(p, PETSC_TRUE, set, size);
}

/* get INVERSE TRANSITIVE CLOSURE of a point p */
PetscErrorCode Mesh::getInverseClosure(const PetscInt p, PetscInt** set, PetscInt *size){
	return getClosure(p, PETSC_FALSE, set, size);
}

/* Also set degrees of freedom */
void getHeights(DM dm, PetscSection section){	
	/* we can lay out data for a continuous Galerkin P_3 inite element method */
	PetscInt pStart, pEnd, cStart, cEnd, c, vStart, vEnd, v, eStart, eEnd, e;

	DMPlexGetChart(dm, &pStart, &pEnd);
	DMPlexGetHeightStratum(dm, 0, &cStart, &cEnd);
	DMPlexGetHeightStratum(dm, 1, &eStart, &eEnd);
	DMPlexGetDepthStratum (dm, 0, &vStart, &vEnd);
	PetscSectionSetChart(section, pStart, pEnd);
	
	printf("\nHeight 0: ");
	for(c = cStart; c < cEnd; ++c){
		PetscSectionSetDof(section, c, 1);	// Sets the dof (1) associated with a given point (c). 
		printf("%d   ", c);
	}
	
	printf("\nHeight 1: ");
	for(v = vStart; v < vEnd; ++v){
		PetscSectionSetDof(section, v, 1);	// Sets the dof (1) associated with a given point (v). 
		printf("%d   ", v);
	}
	
	printf("\nDepth  0: ");
	for(e = eStart; e < eEnd; ++e){
		PetscSectionSetDof(section, e, 2);	// Sets the dof (2) associated with a given point (e). 
		printf("%d   ", e);
	}printf("\n");
}

/* get n-level neighborhood of a point p */
PetscErrorCode Mesh::getNeighborhood(const PetscInt p, int n, PetscInt** set, PetscInt *size){
	vector<PetscInt> v = getNeighborhood(p, n);
	// vector_to_array(v, set, size);
	(*set) = &v[0];		(*size) = v.size();
		
	return ierr;
}

/* ---------------------------------------------------------------*
 * ------------------ VECTOR INTERFACE METHODS ------------------ *
 * ---------------------------------------------------------------*/

/* get CONE of point p */
vector<PetscInt> Mesh::getCone(const PetscInt p){
	const PetscInt* set=NULL;	PetscInt size;
	getCone(p, &set, &size);
	return vector<PetscInt>(set, set+size); 
}

/* get SUPPORT of point p */
vector<PetscInt> Mesh::getSupport(const PetscInt p){
	const PetscInt* set=NULL;	PetscInt size;
	getSupport(p, &set, &size);
	return vector<PetscInt>(set, set+size); 
}

/* get MEET of point array 'points' */ 
vector<PetscInt> Mesh::getMeet(const PetscInt* points, const PetscInt numPoints){
	const PetscInt* set=NULL;	PetscInt size;
	getMeet(points, numPoints, &set, &size);
	return vector<PetscInt>(set, set+size); 
}
vector<PetscInt> Mesh::getMeet(const vector<PetscInt> points){
	return getMeet(&points[0], points.size());
}
	
/* get JOIN of point array 'points' */
vector<PetscInt> Mesh::getJoin(const PetscInt* points, const PetscInt numPoints){
	const PetscInt* set=NULL;	PetscInt size;
	getJoin(points, numPoints, &set, &size);
	return vector<PetscInt>(set, set+size); 
}
vector<PetscInt> Mesh::getJoin(const vector<PetscInt> points){
	return getJoin(&points[0], points.size());
}

/* get TRANSITIVE CLOSURE of a point t*/
vector<PetscInt> Mesh::getClosure (const PetscInt p, const PetscBool direct){
	PetscInt* set=NULL;	PetscInt size;
	getClosure(p, direct, &set, &size);
	return vector<PetscInt>(set, set+size); 	
}
vector<PetscInt> Mesh::getDirectClosure (const PetscInt p){
	return getClosure(p, PETSC_TRUE);
}
vector<PetscInt> Mesh::getInverseClosure(const PetscInt p){
	return getClosure(p, PETSC_FALSE);
}

/* get DIRECT TRANSITIVE CLOSURE of a point p at DEPTH d */
PetscErrorCode Mesh::getDirectiveClosureDepth(const PetscInt p, const PetscInt d, PetscInt** set, PetscInt *size){
	PetscInt pStart, pEnd;
	ierr = getDirectClosure(p, set, size);    CHKERRQ(ierr);
	ierr = getDepthPoints(d, &pStart, &pEnd); CHKERRQ(ierr);
	rangeFilter(*set, *size, pStart, pEnd);
	
	return ierr;
}

/* get INVERSE TRANSITIVE CLOSURE of a point p at DEPTH d */
PetscErrorCode Mesh::getInverseClosureDepth(const PetscInt p, const PetscInt d, PetscInt** set, PetscInt *size){
	PetscInt pStart, pEnd;
	ierr = getInverseClosure(p, set, size);	  CHKERRQ(ierr);
	ierr = getDepthPoints(d, &pStart, &pEnd); CHKERRQ(ierr);
	rangeFilter(*set, *size, pStart, pEnd);
	
	return ierr;
}

/* get n-level NEIGHBORHOOD of a point p */
vector<PetscInt> Mesh::getNeighborhood(const PetscInt p, int n){
	/* Getting transitive closure of */
	PetscInt depth, dStart, dEnd, dr, dp, size, *set=NULL;
	ierr = DMPlexGetDepth(dm, &depth);// CHKERRQ(ierr);
	
	PetscInt depthStratums[4][2]; 
	
	for(dp=0; dp<=depth; dp++){
		// current depth stratum of points
		ierr = DMPlexGetDepthStratum(dm, dp, &dStart, &dEnd);// CHKERRQ(ierr);
		
		// storing depthStratums
		depthStratums[dp][0] = dStart;
		depthStratums[dp][1] = dEnd;
		
		// stops when p is found
		if (dStart <= p && p < dEnd)	break;
	}
	
	//printf("Point %d is on Depth %d\n", p, dp);
	
	dr = dp-n<0? 0: dp-n;	// max(0, dp-n)	
	//printf("Reference depth %d: %d to %d\n", dr, depthStratums[dr][0], depthStratums[dr][1]);
	
	ierr = DMPlexGetTransitiveClosure(dm, p, PETSC_TRUE, &size, &set);// CHKERRQ(ierr);
	for(PetscInt i=1; i < size; i++)	set[i] = set[2*i];
	//displayArray(set, size);
	
	// points filtered by depth reference
	rangeFilter(set, size, depthStratums[dr][0], depthStratums[dr][1]);
	//displayArray(set, size);	
	
	//PetscInt *set_n, size_n=0; /* neighbors points */
	PetscInt *set_a, size_a; /* auxiliary neighbors points */
	vector<PetscInt> v;
	for(PetscInt i=0; i < size; i++){
		set_a=NULL;
		//printf("\n Looking at the inverse transitive closure of %d in depth %d: ", set[i], dp);
		ierr = DMPlexGetTransitiveClosure(dm, set[i], PETSC_FALSE, &size_a, &set_a); //	CHKERRQ(ierr);
		for(PetscInt i=1; i < size_a; i++)	set_a[i] = set_a[2*i];
		
		rangeFilter(set_a, size_a, depthStratums[dp][0], depthStratums[dp][1]);
		//displayArray(set_a, size_a);
		
		v.insert(v.end(), set_a, set_a+size_a);
	}
	//displayArray(v);
	vector_unique_values(v);
	
	return v;
}

/* max depth */
PetscErrorCode Mesh::getDepth(PetscInt* depth){
	ierr = DMPlexGetDepth(dm, depth); CHKERRQ(ierr);
	return ierr;
}

/* points at DEPTH d */
PetscErrorCode Mesh::getDepthPoints(const PetscInt d, PetscInt* pStart, PetscInt *pEnd){
	ierr = DMPlexGetDepthStratum(dm, d, pStart, pEnd); CHKERRQ(ierr);
	return ierr;
}


/* ---------------------------------------------------------------*
 * ------------------------ LIST METHODS ------------------------ *
 * ---------------------------------------------------------------*/

/* list CONE of point p */
PetscErrorCode Mesh::listCone(const PetscInt p, bool out){
	const PetscInt* set=NULL;	PetscInt size;
	ierr = getCone(p, &set, &size); CHKERRQ(ierr);
	
	if(out)	printf("Cone of %d: ", p);
	displayArray(set, size);
	if(out) printf("\n");	else    printf(" ");
	
	return ierr;
}

/* list SUPPORT of point p */
PetscErrorCode Mesh::listSupport(const PetscInt p){
	const PetscInt* set=NULL;	PetscInt size;
	ierr = getSupport(p, &set, &size); CHKERRQ(ierr);
	
	printf("Support of %d: ", p);
	displayArray(set, size);	printf("\n");
	
	return ierr;
}

/* list MEET of point array 'points' */
PetscErrorCode Mesh::listMeet(const PetscInt* points, const PetscInt numPoints){
	const PetscInt* set=NULL;	PetscInt size;
	ierr = getMeet(points, numPoints, &set, &size); CHKERRQ(ierr);
	
	printf("Meet of ");	displayArray(points, numPoints);	printf(": ");
	displayArray(set, size);	printf("\n");
	
	return ierr;
}
	
/* list JOIN of point array 'points' */
PetscErrorCode Mesh::listJoin(const PetscInt* points, const PetscInt numPoints){
	const PetscInt* set=NULL;	PetscInt size;
	ierr = getJoin(points, numPoints, &set, &size); CHKERRQ(ierr);
	
	printf("Join of ");	displayArray(points, numPoints);	printf(": ");
	displayArray(set, size);	printf("\n");
	
	return ierr;
}

/* list TRANSITIVE CLOSURE of a point t*/
PetscErrorCode Mesh::listClosure(const  PetscInt p, const PetscBool direct){
	PetscInt *set=NULL;	PetscInt size;
	ierr = getClosure(p, direct, &set, &size); CHKERRQ(ierr);
	
	if(direct)	printf("Direct  Transitive Closure of %d: ", p);
	else   		printf("Inverse Transitive Closure of %d: ", p);
	displayArray(set, size);	printf("\n");
	
	return ierr;	
}
PetscErrorCode Mesh::listDirectClosure (const PetscInt p){
	return listClosure(p, PETSC_TRUE);
}
PetscErrorCode Mesh::listInverseClosure(const PetscInt p){
	return listClosure(p, PETSC_FALSE);
}

PetscErrorCode Mesh::listDirectiveClosureDepth(const PetscInt p, const PetscInt d, const char *label){
	PetscInt *set=NULL;	PetscInt size;
	ierr = getDirectiveClosureDepth(p, d, &set, &size); CHKERRQ(ierr);
	printf("%s", label);
	displayArray(set, size);	printf("\n");
	
	return ierr;
}

/* list n-level NEIGHBORHOOD of a point p */
PetscErrorCode Mesh::listNeighborhood(const PetscInt p, const int n){	
	printf("Neighborhood %d of point %d: ", n, p);
	displayArray( getNeighborhood(p, n) );	printf("\n");
	
	return ierr;	
}

/* list points at DEPTH with custom label*/
PetscErrorCode Mesh::listDepthPointsLabel(const PetscInt depth, const char label[]){		
	PetscInt p, pStart, pEnd;
	ierr = DMPlexGetDepthStratum(dm, depth, &pStart, &pEnd); CHKERRQ(ierr);
		
	printf("%s", label);
	for(p = pStart; p < pEnd; ++p)		printf("%d ", p);
	printf("\n");
	
	return ierr;
}

/* list points at DEPTH */
PetscErrorCode Mesh::listDepthPoints(const PetscInt depth){
	char label[10];	sprintf(label, "Depth %d", depth);
	return listDepthPointsLabel(depth, "Depth");
}
/* list VERTICES, EDGES, FACES, CELLS of a the Mesh */
PetscErrorCode Mesh::listVertices(){  return listDepthPointsLabel(0, "Vertices: "); }
PetscErrorCode Mesh::listEdges()   {  return listDepthPointsLabel(1, "Edges: ");	}
PetscErrorCode Mesh::listFaces()   {  return listDepthPointsLabel(2, "Faces: ");    }
PetscErrorCode Mesh::listCells()   {  return listDepthPointsLabel(3, "Cells: ");	}

/* list VERTICES, EDGES, FACES, CELLS of a point p */
PetscErrorCode Mesh::listPointStructure(const PetscInt p, const PetscInt d, const char* label){
	PetscInt *set=NULL, size;
	ierr = getDirectiveClosureDepth(p, d, &set, &size); CHKERRQ(ierr);
	printf("%s of %d: ", label, p);
	displayArray(set, size);	printf("\n");
	
	return ierr;
}
PetscErrorCode Mesh::listPointVertices(const PetscInt p){	return listPointStructure(p, 0, "Vertices");}
PetscErrorCode Mesh::listPointEdges   (const PetscInt p){	return listPointStructure(p, 1, "Edges");	}
PetscErrorCode Mesh::listPointFaces   (const PetscInt p){	return listPointStructure(p, 2, "Faces");	}
PetscErrorCode Mesh::listPointCells   (const PetscInt p){	return listPointStructure(p, 3, "Cells");	}

PetscErrorCode Mesh::describe(){
	printf("\n--------------	BRIEF MESH DESCRIPTION	--------------\n");
	// list points
	printf("\t");	listVertices();
	printf("\t");	listEdges();
	printf("\t");	listFaces();
	printf("\t");	listCells();
	printf("\n");
	
	PetscInt d, depth, p, pStart, pEnd;
	ierr = getDepth(&depth);  CHKERRQ(ierr);
	
	for(d=0; d<=depth; d++){
		switch(d){
			case 0:	  printf("VERTICES: \n"); break;
			case 1:	  printf("EDGES: \n"   ); break;
			case 2:	  printf("FACES: \n"   ); break;
			case 3:	  printf("CELLS: \n"   ); break;
			default:  printf("DEPTH %d \n", d);
		}
		ierr = DMPlexGetDepthStratum(dm, d, &pStart, &pEnd); CHKERRQ(ierr);
		for(p = pStart; p < pEnd; ++p){
			printf("[%d]", p);
			if(d >= 0){ printf("\t"); ierr = listPointVertices(p); CHKERRQ(ierr);	}
			if(d >= 1){ printf("\t"); ierr = listPointEdges   (p); CHKERRQ(ierr);	}
			if(d >= 2){ printf("\t"); ierr = listPointFaces   (p); CHKERRQ(ierr);	}
			if(d >= 3){ printf("\t"); ierr = listPointCells   (p); CHKERRQ(ierr);	}
			printf("\n");
		}
		printf("\n");
	}
	
	printf("VERTICES COORDINATES:\n");
	listVertexCoordinates();
	
	return ierr;
}

PetscErrorCode Mesh::listVertexCoordinates(){
	PetscInt numPoints=25, dim=2;
	Vec v;
	VecCreate(PETSC_COMM_WORLD, &v );		// Creates an empty vector object.
	VecSetSizes(v, numPoints*dim, PETSC_DETERMINE);	// Sets the local and global sizes, and checks to determine compatibility
	VecSetFromOptions(v);				//Configures the vector from the options database.

	DMGetCoordinates(dm, &v);
	//DMGetCoordinatesLocal(dm, &v);
	printf("Coordinates: \n");
	ierr = VecView(v, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
	
	/*
	PetscScalar *a;	
	VecGetArray(v, &a);
	printf((float)a);*/
		
	return ierr;
}
