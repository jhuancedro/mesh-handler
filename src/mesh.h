/**---------------------------------------------------------------------
 *    Jhuan B. S. Cedro, June, 2018
 *--------------------------------------------------------------------*/

#include "aux.h"

/* PETSc includes*/
#include <petscdmplex.h>
#include <petscdm.h>
#include <petscdmlabel.h>     
#include <petscds.h>

class Mesh{
private:
	DM dm;
	PetscErrorCode ierr;
	
	PetscErrorCode listDepthPointsLabel(const PetscInt depth, const char label[]);
public:
	Mesh(const char* file);
	~Mesh();
	
	/* ------------------------ GET METHODS ------------------------ */
	
	/* get CONE of point p */
	PetscErrorCode getCone(const PetscInt p, const PetscInt** cone, PetscInt *size);

	/* get SUPPORT of point p */
	PetscErrorCode getSupport(const PetscInt p, const PetscInt** support, PetscInt *size);
	
	/* get MEET of point array 'points' */
	PetscErrorCode getMeet(const PetscInt* points, const PetscInt numPoints, const PetscInt** set, PetscInt *size);
		
	/* get JOIN of point array 'points' */
	PetscErrorCode getJoin(const PetscInt* points, const PetscInt numPoints, const PetscInt** set, PetscInt *size);

	/* get TRANSITIVE CLOSURE of a point p*/
	PetscErrorCode getClosure       (const PetscInt p, PetscBool direct, PetscInt** set, PetscInt *size);
	PetscErrorCode getDirectClosure (const PetscInt p, PetscInt** set, PetscInt *size);
	PetscErrorCode getInverseClosure(const PetscInt p, PetscInt** set, PetscInt *size);
	
	/* get TRANSITIVE CLOSURE of a point p at DEPTH d */
	PetscErrorCode getDirectiveClosureDepth(const PetscInt p, const PetscInt d, PetscInt** set, PetscInt *size);
	PetscErrorCode getInverseClosureDepth  (const PetscInt p, const PetscInt d, PetscInt** set, PetscInt *size);

	/* get n-level NEIGHBORHOOD of a point p */
	PetscErrorCode getNeighborhood(const PetscInt p, int n, PetscInt** set, PetscInt *size);
	
	/* max depth */
	PetscErrorCode getDepth(PetscInt* depth);
	/* points at DEPTH d */
	PetscErrorCode getDepthPoints(const PetscInt depth, PetscInt* pStart, PetscInt *pEnd);
	
	/* TODO: array of coordinates ot the mesh points */
	PetscErrorCode getVertexCoordinates();
	
	/* ------------------ VECTOR INTERFACE METHODS ------------------ */
	
	/* get CONE of point p */
	vector<PetscInt> getCone(const PetscInt p);

	/* get SUPPORT of point p */
	vector<PetscInt> getSupport(const PetscInt p);
	
	/* get MEET of point array 'points' */
	vector<PetscInt> getMeet(const PetscInt* points, PetscInt numPoints);
	vector<PetscInt> getMeet(const vector<PetscInt> points);
		
	/* get JOIN of point array 'points' */
	vector<PetscInt> getJoin(const PetscInt* points, PetscInt numPoints);
	vector<PetscInt> getJoin(const vector<PetscInt> points);

	/* get TRANSITIVE CLOSURE of a point p*/
	vector<PetscInt> getClosure       (const PetscInt p, PetscBool direct);
	vector<PetscInt> getDirectClosure (const PetscInt p);
	vector<PetscInt> getInverseClosure(const PetscInt p);

	/* get n-level NEIGHBORHOOD of a point p */
	vector<PetscInt> getNeighborhood(const PetscInt p, int n);
	
	/* points at DEPTH d */
	vector<PetscInt> getDepthPoints(const uint d);
	
	/* ------------------------ LIST METHODS ------------------------ */
	
	/* list CONE of point p */
	PetscErrorCode listCone(const PetscInt p, bool out=true);

	/* list SUPPORT of point p */
	PetscErrorCode listSupport(const PetscInt p);
	
	/* list MEET of point array 'points' */
	PetscErrorCode listMeet(const PetscInt* points, const PetscInt numPoints);
		
	/* list JOIN of point array 'points' */
	PetscErrorCode listJoin(const PetscInt* points, const PetscInt numPoints);

	/* list TRANSITIVE CLOSURE of a point t*/
	PetscErrorCode listClosure       (const PetscInt p, const PetscBool direct);
	PetscErrorCode listDirectClosure (const PetscInt p);
	PetscErrorCode listInverseClosure(const PetscInt p);
	PetscErrorCode listDirectiveClosureDepth(const PetscInt p, const PetscInt d, const char *label="");

	/* list n-level NEIGHBORHOOD of a point p */
	PetscErrorCode listNeighborhood(const PetscInt p, int n);
	
	/* list points at DEPTH */
	PetscErrorCode listDepthPoints(const PetscInt depth);
	
	/* list VERTICES, EDGES, FACES, CELLS of a the Mesh */
	PetscErrorCode listVertices();
	PetscErrorCode listEdges();
	PetscErrorCode listFaces();
	PetscErrorCode listCells();
	
	/* list VERTICES, EDGES, FACES, CELLS of a point p */
	PetscErrorCode listPointStructure(const PetscInt p, const PetscInt d, const char* label);
	PetscErrorCode listPointVertices(const PetscInt p);
	PetscErrorCode listPointEdges   (const PetscInt p);
	PetscErrorCode listPointFaces   (const PetscInt p);
	PetscErrorCode listPointCells   (const PetscInt p);
		
	/* Brief Mesh describe */
	PetscErrorCode describe();
	
	PetscErrorCode listVertexCoordinates();

};

