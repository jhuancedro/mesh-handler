#include <petscsys.h>
#include <stdio.h>
#include <string.h> 
#include <vector>
#include <algorithm>

typedef unsigned int uint;
using namespace std;

template <typename T>
void displayArray(const T v, uint n, uint step=1){
	printf("{");
	for(uint i=0; i<n; i+=step)
		printf("%d ", v[i]);	
	printf("}");
}

template <typename T>
void displayArray(vector<T> vec, uint step=1){
	displayArray(vec, vec.size(), step);
}

template <typename T>
void intersect(T* v1, uint n1, T* v2, uint n2, T* vu, uint& nu){
	vector<int> v(n1+n2);
	vector<int>::iterator it;

	sort(v1, v1+n1);
	sort(v2, v2+n2);

	it=set_intersection(v1, v1+n1, v2, v2+n2, v.begin());
	v.resize(it-v.begin());

	printf("The intersection has %d elements:\n", v.size());
	for (it=v.begin(); it!=v.end(); ++it)
		printf(" %d");
	printf("\n");
	
	vu = &v[0];
	nu = v.size();
}

template <typename T>
void vector_to_array(vector<T> vec, T* &v, int &n){
	n = vec.size();
	v = new T[n];
	for (PetscInt i = 0; i < n; i++)
		v[i] = vec[i];	
}
/*
template <typename T, typename U>
void vector_to_array(vector<T> vec, T* &v, U &n){
	uint n_;
	vector_to_array(vec, v, n);
	n = (U&) n_;
}*/

template <typename T>
void vector_unique_values(vector<T> &v){	
	sort(v.begin(), v.end());
	typename vector<T>::iterator it;
	it = unique(v.begin(), v.end());
	v.resize( std::distance(v.begin(),it) );
}

template <typename T>
void rangeFilter(T*& v, int& n, T inf, T sup){
	vector<T> vec;
	for(PetscInt i = 0; i < n; i++)
		if(inf <= v[i] && v[i] < sup)
			vec.push_back(v[i]);
	
	//delete [] v;
	vector_to_array(vec, v, n);
}
