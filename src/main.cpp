static char help[] = "Main test\n\n";

#include "mesh.h"

void someTests(Mesh m){
	printf("------------ TEST EXAMPLE -----------\n");	
	m.listVertices();
	m.listEdges();
	m.listFaces();
	m.listCells();
	
	m.listPointVertices(0);
	m.listPointEdges(0);
	m.listPointFaces(0);
	m.listPointCells(0);
	
	m.listCone(9);
	m.listCone(1);
	
	m.listSupport(9);
	m.listSupport(10);
	
	const PetscInt v[] = {2 ,  3};	m.listMeet(v, 2);
	const PetscInt u[] = {15, 17};	m.listJoin(u, 2);
	
	m.listClosure(17, PETSC_TRUE);
	m.listClosure(17, PETSC_FALSE);
		
	m.listDirectClosure(19);
	m.listInverseClosure(19);
	
	m.listNeighborhood(0, 2);
	m.listNeighborhood(14, 2);
	m.listNeighborhood(23, 2);

	// m.listVertexCoordinates();
}

int main(int argc,char **argv){
	PetscErrorCode ierr;
	ierr = PetscInitialize(&argc, &argv, NULL,help);	if (ierr) return ierr;
	
	Mesh m("mesh_examples/malha01.msh");
	someTests(m);
	m.describe();
	
	m.~Mesh();
	ierr = PetscFinalize();
	return ierr;
}
