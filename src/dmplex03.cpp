static char help[] = "Teste 01\n\n";

#include <stdio.h>
#include <string.h>
#include <petscdmplex.h>
#include <petscdm.h>
#include <petscdmlabel.h>     
#include <petscds.h> 
#include <vector>
#include <algorithm>
#include <iostream>

typedef unsigned int uint;
using namespace std;

template <typename T>
void displayArray(const T* array, uint n, uint step=1){
	printf("{");
	for(uint i=0; i<n; i+=step)
		printf("%d ", array[i]);	
	printf("}");
}

template <typename T>
void intersect(T* v1, uint n1, T* v2, uint n2, T* vu, uint& nu){
	vector<int> v(n1+n2);
	vector<int>::iterator it;

	sort(v1, v1+n1);
	sort(v2, v2+n2);

	it=set_intersection(v1, v1+n1, v2, v2+n2, v.begin());
	v.resize(it-v.begin());

	cout << "The intersection has " << (v.size()) << " elements:\n";
	for (it=v.begin(); it!=v.end(); ++it)
		cout << ' ' << *it;
	cout << '\n';
	
	vu = &v[0];
	nu = v.size();
}

template <typename T>
void vector_to_array(vector<T> vec, T* &v, PetscInt &n){
	n = vec.size();
	v = new T[n];
	for (PetscInt i = 0; i < n; i++)
		v[i] = vec[i];	
}

template <typename T>
void vector_unique_values(vector<T> &v){	
	sort(v.begin(), v.end());
	typename vector<T>::iterator it;
	it = unique(v.begin(), v.end());
	v.resize( std::distance(v.begin(),it) );
}

template <typename T>
void rangeFilter(T* &v, PetscInt& n, T inf, T sup){
	vector<T> vec;
	for(PetscInt i = 0; i < n; i++)
		if(inf <= v[i] && v[i] < sup)
			vec.push_back(v[i]);
	
	//delete [] v;
	vector_to_array(vec, v, n);
}

/* Cone and Support auxiliary function */
PetscErrorCode listFunction1(	
  PetscErrorCode (*DMPlexGetFunction    )(DM, PetscInt, const PetscInt**),
  PetscErrorCode (*DMPlexGetFunctionSize)(DM, PetscInt, PetscInt*),
  const char functionName[30], DM dm, PetscInt p, bool out=true){
	const PetscInt *set = NULL;
	PetscInt size;
	PetscErrorCode ierr;
	
	ierr = DMPlexGetFunctionSize(dm, p, &size); CHKERRQ(ierr);
	ierr = DMPlexGetFunction(dm, p, &set); CHKERRQ(ierr);
	
	if(out)	printf("%s of %d: ", functionName, p);
	displayArray(set, size);
	if(out) printf("\n");
	else    printf(" ");
	
	return ierr;
}

/* Meet and Join auxiliary function */
PetscErrorCode listFunction2(
  PetscErrorCode (*DMPlexGetFunction)(DM, PetscInt, const PetscInt*, PetscInt*, const PetscInt**),
  const char functionName[30],  DM dm, PetscInt numPoints, const PetscInt* points){
	const PetscInt *set = NULL;
	PetscInt size;
	PetscErrorCode ierr;
	
	ierr = DMPlexGetFunction(dm, numPoints, points, &size, &set); CHKERRQ(ierr);
	
	printf("%s of ", functionName);
	displayArray(points, numPoints);
	printf(": ");
	displayArray(set, 2*size, 2);
	printf("\n");
	
	return ierr;
}

/* list CONE of point p */
PetscErrorCode listCone(DM dm, PetscInt p, bool out=true){
	return listFunction1(DMPlexGetCone, DMPlexGetConeSize, "Cone", dm, p, out);
}

/* list SUPPORT of point p */
PetscErrorCode listSupport(DM dm, PetscInt p, bool out=true){
	return listFunction1(DMPlexGetSupport, DMPlexGetSupportSize, "Support", dm, p, out);
}

/* list MEET of point array 'points' */
PetscErrorCode listMeet(DM dm, PetscInt numPoints, const PetscInt* points){
	return listFunction2(DMPlexGetMeet, "Meet", dm, numPoints, points);
}

/* list JOIN of point array 'points' */
PetscErrorCode listJoin(DM dm, PetscInt numPoints, const PetscInt* points){
	return listFunction2(DMPlexGetJoin, "Join", dm, numPoints, points);
}

/* list TRANSITIVE CLOSURE of a point t*/
PetscErrorCode listClosure(DM dm, PetscInt p, PetscBool direct=PETSC_TRUE){
	PetscInt *set = NULL;
	PetscInt size;
	PetscErrorCode ierr;
	
	ierr = DMPlexGetTransitiveClosure(dm, p, direct, &size, &set); CHKERRQ(ierr);
	if(direct)	printf("Direct  Transitive Closure of %d: ", p);
	else   		printf("Inverse Transitive Closure of %d: ", p);
	
	displayArray(set, 2*size, 2);
	printf("\n");
	
	return ierr;
}

/* Also set degrees of freedom */
void listHeights(DM dm, PetscSection section){	
	/* we can lay out data for a continuous Galerkin P_3 inite element method */
	PetscInt pStart, pEnd, cStart, cEnd, c, vStart, vEnd, v, eStart, eEnd, e;

	DMPlexGetChart(dm, &pStart, &pEnd);
	DMPlexGetHeightStratum(dm, 0, &cStart, &cEnd);
	DMPlexGetHeightStratum(dm, 1, &eStart, &eEnd);
	DMPlexGetDepthStratum (dm, 0, &vStart, &vEnd);
	PetscSectionSetChart(section, pStart, pEnd);
	
	printf("\nHeight 0: ");
	for(c = cStart; c < cEnd; ++c){
		PetscSectionSetDof(section, c, 1);	// Sets the dof (1) associated with a given point (c). 
		printf("%d   ", c);
	}
	
	printf("\nHeight 1: ");
	for(v = vStart; v < vEnd; ++v){
		PetscSectionSetDof(section, v, 1);	// Sets the dof (1) associated with a given point (v). 
		printf("%d   ", v);
	}
	
	printf("\nDepth  0: ");
	for(e = eStart; e < eEnd; ++e){
		PetscSectionSetDof(section, e, 2);	// Sets the dof (2) associated with a given point (e). 
		printf("%d   ", e);
	}printf("\n");
}

/* list DEPTHS of a section */
PetscErrorCode listDepths(DM dm, bool show_cone=false){	
	PetscInt depth, pStart, pEnd, p, i;
	PetscErrorCode ierr;
	ierr = DMPlexGetDepth(dm, &depth); CHKERRQ(ierr);
	
	for(i=0; i<=depth; i++){
		ierr = DMPlexGetDepthStratum(dm, i, &pStart, &pEnd); CHKERRQ(ierr);
		
		printf("Depth  %d: ", i);
		for(p = pStart; p < pEnd; ++p){
			printf("%d ", p);
			if(show_cone and i > 0){
				listCone(dm, p, false);
			}
		}
		printf("\n");
	}
	
	return ierr;
}

/* list n-level neighborhood of a point p */
PetscErrorCode listNeighborhood(DM dm, PetscInt p, int n){
	/* Getting transitive closure of */
	PetscInt depth, dStart, dEnd, dr, dp, size, *set=NULL;
	PetscErrorCode ierr;
	ierr = DMPlexGetDepth(dm, &depth); CHKERRQ(ierr);
	
	PetscInt depthStratums[4][2]; 
	
	for(dp=0; dp<=depth; dp++){
		// current depth stratum of points
		ierr = DMPlexGetDepthStratum(dm, dp, &dStart, &dEnd); CHKERRQ(ierr);
		
		// storing depthStratums
		depthStratums[dp][0] = dStart;
		depthStratums[dp][1] = dEnd;
		
		// stops when p is found
		if (dStart <= p && p < dEnd)	break;
	}
	
	//printf("Point %d is on Depth %d\n", p, dp);
	
	dr = dp-n<0? 0: dp-n;	// max(0, dp-n)	
	//printf("Reference depth %d: %d to %d\n", dr, depthStratums[dr][0], depthStratums[dr][1]);
	
	ierr = DMPlexGetTransitiveClosure(dm, p, PETSC_TRUE, &size, &set); CHKERRQ(ierr);
	for(PetscInt i=1; i < size; i++)	set[i] = set[2*i];
	//displayArray(set, size);
	
	// points filtered by depth reference
	rangeFilter(set, size, depthStratums[dr][0], depthStratums[dr][1]);
	//displayArray(set, size);	
	
	PetscInt *set_n, size_n=0; /* neighbors points */
	PetscInt *set_a, size_a; /* auxiliary neighbors points */
	vector<PetscInt> v;
	for(PetscInt i=0; i < size; i++){
		set_a=NULL;
		//printf("\n Looking at the inverse transitive closure of %d in depth %d: ", set[i], dp);
		ierr = DMPlexGetTransitiveClosure(dm, set[i], PETSC_FALSE, &size_a, &set_a); CHKERRQ(ierr);
		for(PetscInt i=1; i < size_a; i++)	set_a[i] = set_a[2*i];
		
		rangeFilter(set_a, size_a, depthStratums[dp][0], depthStratums[dp][1]);
		//displayArray(set_a, size_a);
		
		v.insert(v.end(), set_a, set_a+size_a);
	}
	vector_unique_values(v);
	vector_to_array(v, set_n, size_n);
	
	printf("Neighborhood %d of point %d: ", n, p);
	displayArray(set_n, size_n);
	printf("\n");
	
	return ierr;
}

PetscErrorCode listVertexCoordinates(DM dm){
	PetscErrorCode ierr;
	PetscInt numPoints=25, dim=2;
	Vec v;
	VecCreate(PETSC_COMM_WORLD, &v );		// Creates an empty vector object.
	VecSetSizes(v, numPoints*dim, PETSC_DETERMINE);	// Sets the local and global sizes, and checks to determine compatibility
	VecSetFromOptions(v);				//Configures the vector from the options database.

	DMGetCoordinates(dm, &v);
	//DMGetCoordinatesLocal(dm, &v);
	printf("Coordinates: \n");
	ierr = VecView(v, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
	
	/*
	PetscScalar *a;	
	VecGetArray(v, &a);
	printf((float)a);*/
		
	return ierr;
}

int main(int argc,char **argv){
	DM             dm;
	PetscInt       dim = 1;
	PetscBool      interpolate = PETSC_TRUE;
	PetscErrorCode ierr;

	ierr = PetscInitialize(&argc, &argv, NULL,help);if (ierr) return ierr;
	//ierr = PetscOptionsGetInt(NULL,NULL, "-dim", &dim, NULL);CHKERRQ(ierr);
	
	/* Importing mesh from gmsh file */
	ierr = DMPlexCreateFromFile(PETSC_COMM_WORLD, "mesh_examples/malha01.msh", interpolate, &dm); CHKERRQ(ierr);
	
	PetscInt pStart, pEnd;
	DMPlexGetChart(dm, &pStart, &pEnd);
	printf("malha importada com points de %d a %d \n", pStart, pEnd);
	
	listDepths(dm);
	
	listCone(dm, 9);
	listCone(dm, 1);
	
	listSupport(dm, 9);
	listSupport(dm, 10);
	
	listClosure(dm, 17, PETSC_TRUE);
	listClosure(dm, 17, PETSC_FALSE);
	
	const PetscInt m[] = {2, 3};
	listMeet(dm, 2, m);
	
	const PetscInt j[] = {15, 17};
	listJoin(dm, 2, j);
		
	listClosure(dm, 19, PETSC_TRUE);
	listClosure(dm, 19, PETSC_FALSE);
	
	listNeighborhood(dm, 0, 2);
	listNeighborhood(dm, 14, 2);
	listNeighborhood(dm, 23, 2);
	
	listVertexCoordinates(dm);
		
	
	// ierr = DMDestroy(&dm);CHKERRQ(ierr); // Segmentation fault (core dumped)
	ierr = PetscFinalize();
	return ierr;
}
